#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# IDG_Dream_Project
# Copyright (C) 2018  IDG_Dream

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import pandas as pd
import numpy as np

# Read csv

ds = pd.read_csv('../../../data/Processed/Model_Creation/1-After_KD_Selection/KD_dataset.csv')

# Remove unnecessary columns 

ds = ds[['compound_id', 'standard_inchi_key', 'target_id', 'standard_type', 'standard_value', 'standard_units']] 

# Remove NaN present BOTH in compound_id and standard_inchi_key columns

ds = ds.dropna(subset=['compound_id', 'standard_inchi_key'], how='all')

# Remove NAN from the columns target_id, standard_value and standard_units

ds = ds.dropna(subset=['target_id', 'standard_value', 'standard_units'])

# Remove where there are multiple target_id

ds = ds[ds['target_id'].str.len() <= 6]

# Remove wrong units

ds = ds[ds["standard_units"].isin([ 'MM', '-LOG(10) M', 'NM' , 'NMOL/L'])] 

# Remove rows with 0 in column standard_value before converting

ds = ds[ds['standard_value'] != 0]

# Convert units to -log10(M)

def minus_log_decorator(func):
    def minus_log_wrapper(y):
        return -np.log10(func(y))
    return minus_log_wrapper

@minus_log_decorator
def convert(x):
    if x[1] == 'MM':
        return 10**-6*x[0]
    elif x[1] == '-LOG(10) M':
        return 10**-x[0] 
    elif x[1] == 'NM' or x[1] == 'NMOL/L':
        return 10**-9*x[0]

ds['standard_value'] = ds[['standard_value', 'standard_units']].apply(convert, axis=1)

# Remove rows with KDS and KD/KI in the column standard_value and then remove the column

ds = ds[~ds['standard_type'].isin(['KDS', 'KD/KI'])] 

# Remove the columns standard_units and standard_type

ds = ds.drop(['standard_units', 'standard_type'], axis=1)

# add the columns and information of standard_inchi and canonical_smiles

ds = ds.assign(canonical_smiles = np.NaN)
ds = ds.assign(standard_inchi = np.NaN)

with open('../../../data/Preprocessed/ChEMBL/chembl_24_1_chemreps.txt', 'r') as f:
    next(f)
    comp_id_no_st_key = ds.compound_id.dropna().values
    st_key_no_comp_id = ds[ds['compound_id'].isnull()].standard_inchi_key.values
    for line in f:
        line = line.split()
        if line[0] in comp_id_no_st_key:
            if len(line) == 3:
                ds.loc[ds['compound_id'] == line[0], 'canonical_smiles'] = line[1]
            elif len(line) == 4:
                ds.loc[ds['compound_id'] == line[0], 'standard_inchi'] = line[2]
                ds.loc[ds['compound_id'] == line[0], 'canonical_smiles'] = line[1]
        elif len(line) == 3 and line[2] in st_key_no_comp_id:
                ds.loc[ds['standard_inchi_key'] == line[2], 'canonical_smiles'] = line[1]
        elif len(line) == 4 and line[3] in st_key_no_comp_id:
                ds.loc[ds['standard_inchi_key'] == line[3], 'standard_inchi'] = line[2]
                ds.loc[ds['standard_inchi_key'] == line[3], 'canonical_smiles'] = line[1]

# Drop NaNs in standard_inchi and canonical_smiles

ds = ds.dropna(subset=['standard_inchi', 'canonical_smiles'], how='all')

# Drop the column compound_id

ds = ds.drop(['compound_id', 'standard_inchi_key'], axis=1)

# Load human proteome dataset

ds_prot = pd.read_csv('../../../data/Preprocessed/Uniprot/human_proteome.tab', sep='\t')

# Rename column of ds_prot

ds_prot.rename(columns={'Entry':'target_id'}, inplace=True)

# Merge the to dataset

merged_ds = pd.merge(ds, ds_prot, how='inner', on=['target_id'])

# Drop target_id column

merged_ds = merged_ds.drop(['target_id'], axis=1)

# Save dataset

merged_ds.to_csv('../../../data/Processed/Model_Creation/2-After_Data_Preparation/ds_prep.csv', index=False)

