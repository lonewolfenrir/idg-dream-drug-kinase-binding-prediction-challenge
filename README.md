# IDG-DREAM Drug-Kinase Binding Prediction Challenge #

## Introduction ##

Kinases are protein enzymes responsible for the reversible phosphorylation in proteins. This mechanism is responsible for function regulation in many proteins of the proteome. After the completion of the human genome nearly all of the human kinome was identified. The total amount of kinases identified through this process was 518.

Protein kinases work by changing substrate activity. This modification regulates many processes in the cell, like metabolism, transcription, cell cycle progression, cytoskeletal rearrangement, cell movement, apoptosis and differentiation. Because of such important functions on the organism homeostasis, mutations on the genes encoding kinases usually lead to Human disease. Therefore, agonists and antagonists of these proteins are important targets for the development of many new therapeutics.

The IDG-DREAM Drug-Kinase Binding Prediction Challenge main purpose is to use machine learning, statistical and other computational tools to predict the interaction between kinase inhibitors, an important drug class, and their targets. Such approaches in drug discovery and effectiveness are important since they reduce the amount of large expenses related to drug discovery and reduce the time needed in this process.

To fulfil this challenge a machine learning model is going to be developed with a dataset that contains curated KD values. The model will be then used to predict the pKD of the round_1_template.csv dataset provided by the IDG-DREAM Drug-Kinase Binding Prediction Challenge. Two iterations of the model will be run, each one with different features.

The python scripts developed in this work are attached as a compressed file to this report. There are two directories, which correspond to iteration 1 and 2. Inside these directories src and data directories can be found. The src contains the directories Model_Creation and pKD_Prediction, where the scripts to make the machine learning model and the pKD prediction are stored, respectively. In data, all the files used to make the model and predictions are stored.

## Model_Creation ##

### Raw Data Selection ###

Collecting the right information is the most crucial step in machine learning. In this case, KD values of drug-kinase interactions have to be precise and accurate so their prediction is also trustworthy.

The wiki of the challenge suggests using the data of the website DrugTargetCommons, a crowd-sourcing platform to improve the consensus and use of drug-target interactions, as training data. As such, the dataset was downloaded using the UNIX command wget:

```bash
wget https://drugtargetcommons.fimm.fi/static/Excell_files/DTC_data.csv  
```

Only the rows containing any form of KD in the column standard_type of the downloaded dataset were selected, as the column standard_type contains several measures that cannot be converted to KD values. This was achieved by using the following UNIX commands:

```bash
egrep ',[^,]*[Kk]{1}[Dd]{1}[^,"]*,(=|>){1},' ../../../data/Preprocessed/DrugTargetCommons/DTC_data.csv > ../../../data/Processed/Model_Creation/1-After_KD_Selection/KD_dataset.csv 
sed -i '1i compound_id,standard_inchi_key,compound_name,synonym,target_id,target_pref_name,gene_names,wildtype_or_mutant,mutation_info,pubmed_id,standard_type,standard_relation,standard_value,standard_units,ep_action_mode,assay_format,assaytype,assay_subtype,inhibitor_type,detection_tech,assay_cell_line,compound_concentration_value,compound_concentration_value_unit,substrate_type,substrate_relation,substrate_value,substrate_units,assay_description,title,journal,doc_type,annotation_comments' ../../../data/Processed/Model_Creation/1-After_KD_Selection/KD_dataset.csv  
```

The dataset at this point of time contained a total of 83083 rows and 32 columns.

### Data Preparation ###

Data preparation is essential whenever raw data is given. This step aims to refine raw data into information that can be used in the analysis or as training data for machine learning modelling.

First, after a thorough study of the raw dataset, only a few columns were selected for further analysis: compound_id, standard_inchi_key, target_id, standard_type, standard_value and standard_units.

The columns compound_id and standard_inchi_key were selected because their information allows to add the additional columns standard_inchi and canonical_smiles, which will be used to add compound features.

The target_id column contains uniprot IDs that will be used to retrieve and then insert the corresponding protein sequences into a column. Thereafter, protein features based on sequences will be added.

The most important column is standard_value which contains the KD values. The remaining two columns, standard_type and standard_units, allow the conversion of the KD values to the right units.

The next step of preparing the dataset was removing NaNs. For the columns compound_id and standard_inchi_key, 3305 and 2996 NaNs were found, respectively. However, only one of these columns is necessary to add the additional columns, therefore only 1756 rows with NaN in both these columns were removed. 2281, 16, 586 and 0 NaNs were found in the columns target_id, standard_value, standard_units and standard_type, respectively. These NaNs can not be filled with the information present in the dataset, as a result they were removed.

While dealing with the NaNs, inconsistencies and problems in the dataset were detected. One of the problems in the dataset were the multiple kinase IDs in the column target_id. This is due to the fact that the assay did not specify which kinase was used, so the IDs of all possible kinases were enlisted. If the correct drug-kinase interaction is unknown, all these rows must be deleted. In total 3169 rows were removed from the dataset. An inconsistency in the data appeared in the columns standard_value and standard_units. The values in the column standard_value had different units as it is possible to see in the table below.

| Units       | Occurrences |
|-------------|-------------|
| NM          | 74364       |
| -LOG(10) M  | 668         |
| 10'-1/S     | 49          |
| 10'-2/S     | 40          |
| 10'-3/S     | 33          |
| MIN-1       | 22          |
| M           | 20          |
| NMOL/L      | 19          |
| /S          | 16          |
| S-1         | 13          |
| MM          | 13          |
| /UM         | 8           |
| UG ML-1     | 6           |
| 10'-4/S     | 5           |
| UL/OD/MIN   | 3           |
| NO_UNIT     | 2           |
| KCAL/MOL    | 2           |
| 10'-9L/MOL  | 1           |
| 10'3/S      | 1           |
| 10'-6/S     | 1           |
| 1/S         | 1           |
| 10'-10L/MOL | 1           |
| 10'1/S      | 1           |
| 10'-5/S     | 1           |

To have a consistent dataset, the values have to be in the same unit, Molar. Thus, the values in standard_value column were converted to Molar. It was only possible to convert the MM, -LOG(10) M, NM and NMOL/L units. Other units needed additional information to make this conversion, unfortunately that information was not available and as such 226 rows were removed.

Then, the values were converted to pKD using -log10. As the values before the conversion cannot be equal to 0, rows containing a 0 were eliminated. Only 1 row was eliminated.

In the column standard_type, inconsistencies were also observed as it contains several KD types, which are possible to see in the table below.

| Type     | occurrences |
|----------|-------------|
| KD       | 67304       |
| KDAPP    | 5137        |
| Kd       | 1798        |
| PKD      | 668         |
| KD/KI    | 61          |
| KD1      | 33          |
| KD2      | 31          |
| KDISS    | 15          |
| KD APP   | 11          |
| KD HYDRO | 4           |
| KDSPR    | 1           |

To understand these various units types, publications related to the values were analysed. Only the KD/KI type, 61 rows, was removed due to the fact that the KI value is unknown and therefore it can not be converted to pKD. The remaining types represent the same, the dissociation constant. Since the columns standard_units and standard_type were not required for further data preparation and analysis they were removed.

To add the canonical_smiles and standard_inchi columns, a file containing these different chemical representations was downloaded from the database ChEMBL, a manually curated chemical database of bioactive molecules with drug-like properties. To download and extract the file the following UNIX commands were used:

```bash
wget http://ftp.ebi.ac.uk/pub/databases/chembl/ChEMBLdb/releases/chembl_24_1/chembl_24_chemreps.txt.gz
gunzip chembl_24_chemreps.txt.gz
```

The information in the columns compound_id and standard_inchi_key was used to add the additional columns. Not all values were set in the two additional columns, because 660 and 655 NaNs were found in standard_inchi and canonical_smiles, respectively. Fortunately, compound features can be added using only one of these columns, therefore only 655 rows that contained NaN in both columns were removed. The columns compound_id and standard_inchi_key were removed because they are not longer needed.

The kinases studied in this challenge are from Homo Sapiens so the human proteome was downloaded from Uniprot as a tab separated file with two columns: Entry and Sequence. The column Entry was renamed as target_id. Then the dataset of the proteome was inner joined on the column target_id with the previous dataset to add the Sequence column. Since, the target_id column will not be required, it was removed, resulting in a merged dataset of 66618 rows and 4 columns. These number of rows can be explained by the fact that the previous dataset contained non-human kinases.

The code for data preparation is available in the file src/Model_Creation/2-Data_Preparation/data_preparation.py.

### Adding Features ###

To make a supervised machine learning model it is required an input of numerical vectors. For this reason, the protein and drug compound sequences must be replaced with sequences of numeric symbols that represent and describe proteins or drug compounds physicochemical properties. To add such sequences the packages Biopython, PyDPI and RDKIT that allow for the automatic calculus of a large amount of both compound and proteins features were used. The scripts to add the features are in the directory src/Model_Creation/3-Feature_Selection.

#### Protein Features ####

The features were added to the dataset using the Bio.SeqUtils. ProtParam module of the BioPython and PyDPI (Drug - Protein Interaction with python) packages. PyDPI allows to compute structural and physicochemical features of proteins and peptides from amino acid sequences. Both packages require the protein sequences that is present in the column “Sequence” of the dataset.

Biopython ProtParam tools were used to calculate isoelectric point, molecular weight, aromaticity and average of hydropathy.

Isoelectric point is the value of pH at which a molecule is electrically neutral, bearing no electrical charge.

Molecular weight is the value of the sum of the relative atomic masses of a given molecule.

Aromaticity is related to the presence of ring shaped molecules inside a structure. These structures confer stability to a molecule, thus the higher the value of aromaticity the higher the stability of the molecule. Instability index is a measure related to the overall protein stability.

Grand average of hydropathy (GRAVY) is a measure that sums the overall hydropathy of the constituents of the sequence of the protein.

PyDPI was used to calculate amino acid composition (AAC), Pseudo Amino acid Composition (PseAAC), composition, transition and distribution (CTD) and Moran autocorrelation descriptors.

PseAAC deals with the amino acid composition, but taking into account sequence order correlation (values of position, hydrophobicity, hydrophilicity or side chain mass of amino acid). Whereas the first 20 components reflect the conventional amino acid composition, the remaining PseAAC components reflect the correlation patterns, hence incorporating sequence order correlation patterns. It has been widely used in problems involving protein and protein related systems. The number of PseAAC descriptors generated is dependent of the metrics used in the functions, here, a dictionary with 30 PseAAC descriptors was generated.

In the CTD feature, the amino acids are divided in three classes according to its attribute, with each amino acid being encoded by a index (1, 2 or 3) according to the class it belongs. It is possible to divide the amino acids by the possible 7 attributes of hydrophobicity, normalized van der Waals volume, polarity, charge, polarizability, charge, secondary structure and solvent accessibility. After setting the amino acid classes, it is calculated the global percentage of each encoded class in the sequence (composition), the percentage frequency with which class is followed by another (e.g. 1 followed by 3 or 3 followed by 1) in the encoded sequence (transition) and the distribution of each attribute in the sequence. The distribution represents the position percentages in the whole sequence for the first residue, 25% residues, 50% residues, 75% residues and 100%residues for a specific encoded class. CTD calculus generates a dictionary with 147 keys (21 from composition, 21 from transition and 105 from distribution).

Autocorrelation descriptors are based on the distribution of amino acid properties along the sequence, which measures the correlation between two residues separated by a distance in terms of their evolution scores. The Moran autocorrelation generates a dictionary of 240 components based on the properties of hydrophobicity scale, average flexibility index, polarizability parameter, free energy of amino acid solution in water, residue accessible surface area, amino acid residue volume, steric parameters and relative mutability.

All the features selected were described in the literature as having good results in protein predictions problems.

### Compound Features ###

To add compound features, the python package RDKIT was used. RDKIT is a toolkit for chemoinformatics that generates fingerprints and molecular descriptors for machine learning.

Molecular fingerprints use a series of binary bits (Boolean array) to encode molecular structure representing the presence (set 1) or absence (set 0) of particular substructures in the molecule. Different molecular fingerprints systems describe different aspects of molecular topology. RDKIT allows to compute Topological fingerprint, E-state fingerprints (define electrotopological state), atom pairs and topological torsion fingerprint, Morgan Circular fingerprint and MAACS fingerprints.

The compounds were described using the MAACS fingerprint, one of the most used fingerprints. MAACS Keys generate 166 bit structural key descriptors in which bit is associated with a substructure pattern, for example, a presence of a phenyl ring, S-S bond or presence of specific atoms (e.g. F) allowing to compare compounds.

Molecular descriptors also allow to represent compounds by capturing distinct aspects of chemical structures. RDKIT has a variety of descriptors from base descriptors (e.g. weight, charge and number of electrons), constitutional molecular descriptors (e.g. the count of specific atoms, the weight of the molecule, number of single or double bonds in molecule, number of atoms, number of H donors and H acceptors or charge), autocorrelation descriptors including Moran autocorrelation descriptors (Returns 2D autocorrelation descriptors vector), molecular connectivity indexes (Chi indexes) and Kappa descriptors and topological descriptors (Bertzct descriptor, Balaban value or ipc index). In total 47 basic, constitutional, connectivity and autocorrelation and topological descriptors calculated with Descriptors, rdMolDescriptors and GraphDescriptors modules were generated. In total were calculated 404 features for compounds.

A total of 847 proteins and compounds features were added to the dataset. However, 2 columns of features contain NaNs or None were removed, resulting in 845 features.

### Machine Learning Modelling ###

In this step, all the data gathered is going to be used to make machine learning models capable of predicting pKD values of drug-kinase interactions.

pKD values of the training data are known, so supervised learning algorithms are going to be use, specifically regression algorithms, since the pKD values are continuous numerical values. The algorithms selected to be tested in a scikit-learn pipeline are the support vector regression (SVR), with the rbf kernel, and Ridge. This selection is based on the flow chart given by scikit-learn.

First, the column standard_value was set to a variable called y, which represents the target. Then, the other columns were set to the variable X except for the columns: standard_value, standard_inchi, canonical_smiles and Sequence. The variable X represent the features. The y and X variables were divided in training and test datasets, the test size was 20% of the whole dataset.

Then, two scikit-learn pipelines were created with a scaler step, which uses the function StandardScaler() and a classifier step, one with SVR and the other with the Ridge algorithm. The SVR was tested using different values for C (1 , 10 and 100) and Gamma (0.001 and 0.0001). In Ridge, only different Alpha values were tested (0.1, 1 and 10). To test the different parameters, GridSearchCV was used with a value of cross-validation of 3. The best algorithm and parameters were selected and dumped using python's pickle module automatically.

To evaluate the regression machine learning models, several error metrics were used: r2 or coefficient of determination, explained variance score, mean absolute error, mean squared error and median absolute error.

The R2 score is a regression metric used to assess the goodness of fit of the regression model. It ranges between 0 and 1 indicating poor fit and perfect fit respectively.

The explained variance score measures the proportion to which a ML model accounts for the variation (dispersion) of a dataset. The best possible score is 1.0, lower values are worse.

The mean absolute error is the average of the difference between the real values and the predictive values. The mean squared error is the average of the square of the difference between real and predictive values. Both metrics range from 0 to infinite with lower values being better than higher ones.

Both mean absolute error and mean squared error are somewhat sensitive to outliers, therefore median absolute error was also used, which is, basically, insensitive to outliers giving a more robust measure of the model performance.

The script containing the code for both ML modelling and evaluation is available in the file src/Model_Creation/4-Model_Persistence/ml_model.py.

## pKD Prediction ##

To predict the pKD of the round 1 dataset, it was necessary to add the sequence in the data preparation step.

Then, to add the features, for each iteration, the process was the same used to create the machine learning models.

To make the actual pKD prediction, the model was loaded with the pickle module and fed with the features previously added. A column with the predictions was added to the original round 1 dataset. The predictions are available in the file data/Processed/KD_Prediction/Round_1/3-After_Model_Prediction/round_1_pred.csv.

## Results ##

To accomplish a better performance of the machine learning models, two iterations were made. The differences between these two iterations were based on the feature addition. The results of both iterations are displayed below.

For the first iteration isoelectric point, molecular weight, aromaticity, gravy, amino acid composition and CTD were calculated for feature protein, compound features were not included. In the second iteration, all the compound features described were added. Additionally, PseAAC and Moran Autocorrelation descriptors were added. AAC was removed, once the PseAAC has already that information on amino acid composition. In the second iteration a total of 443 features were added to the dataset.

### First Iteration ###

The results of the machine learning metrics for SVR and Ridge for the first iteration are shown in the tables 3 and 4 respectively.

For SVR:

SVR_r2_score - 0.24574847521905796

SVR_explained_variance_score - 0.28704651951763405

SVR_mean_absolute_error - 0.6676763246531715

SVR_mean_squared_error - 1.1857157488586791

SVR_median_absolute_error - 0.24600883476015722

For Ridge:

Ridge_r2_score - 0.18506675477005663

Ridge_explained_variance_score - 0.1851839615594476

Ridge_mean_absolute_error - 0.8467971924425742

Ridge_mean_squared_error - 1.2811100162088411

Ridge_median_absolute_error - 0.6392311521644425


As expected, the results show that the model is not a good predictor. Both values of r2 score and explained variance are low whereas the values of the mean absolute error, mean squared error and median absolute error are relatively high. Although the differences are not very significant, the SVR scores are slightly better than Ridge scores. The best parameters for the SVR model were: 100 for C and 0.001 for gamma.

Since only protein features were used, any row with different compounds but with the same proteins were predicted has having the same pKD.

### Second Iteration ###

For the second iteration, considerably more features were added. Besides the already existent protein features, pseudo amino acid composition and Moran autocorrelation were added. Also, compound features were integrated. As such, this second input data became much more complete and descriptive of both kinases and compounds.

The results of the machine learning metrics for SVR and Ridge for the second iteration are shown in the tables 5 and 6 respectively.

For SVR:

SVR_r2_score - 0.5723468001105774

SVR_explained_variance_score - 0.572368113497705 

SVR_mean_absolute_error - 0.375417895084560

SVR_mean_squared_error - 0.36762288653052416 

SVR_median_absolute_error 0.16383650092976554 

For Ridge:

Ridge_r2_score - 0.4334716206739475

Ridge_explained_variance_score - 0.4341401668370085 

Ridge_mean_absolute_error 0.47908585204774495

Ridge_mean_squared_error - 0.48700395124637147

Ridge_median_absolute_error - 0.3134621075154418

In the second iteration the results improved considerably when compared to the first iteration and, therefore, the model can be considered a better predictor. These results were expected as the feature addition leads to a better description of both proteins and compounds. As in the first iteration, the values of the SVR scores are slightly higher than the Ridge scores. The best parameters for SVR model were: 10 for C and 0.001 for gamma.

In conclusion, although the results are positive, there is margin for improvement. A better model could be accomplished by adding feature selection, testing more parameters and machine learning algorithms.
